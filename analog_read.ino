#include <stdarg.h>

int numOfPin = 3;
int analogPin[] = {0, 1, 2};
int value[] = {0, 0, 0};
int valueMemory[] = {0, 0, 0};
int index;
int pin;

void setup() {
  Serial.begin(9600);
}

void loop() {
  delay(100);
  for(index = 0; index < numOfPin; index++){
    pin = analogPin[index];
    value[index] = analogRead(pin);
    
    if (value[index] != valueMemory[index]){
      Serial.println(value[index]);
      valueMemory[index] = value[index];
    }
  }
}
